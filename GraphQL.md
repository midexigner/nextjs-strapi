## GraphQL
- GraphQL is a powerful query language
- Allows for a more flexible & efficient approach than REST

### A RESTful Approach..
- Endpoint for getting a particular book:
  - domain.com/books/:id
  - title,genre,reviews.authorId
- Endpoint for getting the author info of that book:
  - domain.com/authors/:id
  - name,age,biograph.bookIds
- Query to get book data and it's author data (And the other book):
```sh
{
    book(id:123){
        title
        genre
        reviews
        authorId
        author{
            name
            bio
            books{
                name
            }
        }

    }
}
```
### Express App Setup
- mkdir `server` folder
- npm install -g npm
- npm init -y
- npm i express
```sh
const express = require('express')

const app = express();

app.listen(4000,(()=>{
    console.log(`now listening for requests on port`)
}))
```

### Setting up GraphQL 
- npm install graphql express-graphql