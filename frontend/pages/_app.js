import Header from 'components/Header'
import { ThemeProvider,jsx } from '@emotion/react'
// import '../styles/globals.css'
import GlobalStyles from '../GlobalStyles/GlobalStyles'
// import {ThemeProvider } from '@emotion/react'
const theme = {
  colors:{
    primary:'#ff0000'
  }
}

function MyApp({ Component, pageProps }) {
  return (
<>
<ThemeProvider theme={theme}>
<GlobalStyles />
<Header/>
<Component {...pageProps} />
</ThemeProvider>
</>

  )
}

export default MyApp
