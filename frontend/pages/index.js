import fetch from 'isomorphic-unfetch'
import Card from '../components/Card'
 

export default function Home({ movies, t }) {
  // console.log(movies)
  return (
    <div className="container">
    
      {/* <Image src="/images/chewy.jpg" width={2400} height={1600}  /> */}
      <div className="movie__list">
     {movies.map(movie =>(
       <Card key={movie.id} movie={movie} />
     ))}
     </div>
     <style jsx>{`
        .movie__list{
          display:flex;
        }
        `}</style>
    </div>
  )
}

export async function getServerSideProps() {

const { API_URL } = process.env
const res = await fetch(`${API_URL}/movies`)
const data = await res.json()
return {
  props: {
    movies: data
  }, // will be passed to the page component as props
}
}
