import styled from '@emotion/styled'
import { rem } from 'polished'
import Link from 'next/link'

const Header = ({isDark}) => {
    return (
        <HeaderStyled isDark={isDark}>
            <div className="container">
            <div className="logo">
                        <Link href="/">
                            <a>
                                <img src="/images/logo.svg" alt="Sites logo" />
                                <span className="logo-text">Next Movies</span>
                            </a>
                        </Link>
             </div>
            </div>
            
        </HeaderStyled>
    )
}

const HeaderStyled = styled.header`
    background:${props => props.theme.colors.primary};
    background:${props => props.isDark ? '#000000' : '#efefef' };
    padding: ${rem(20)};
    .logo {
        a {
            display: flex;
            align-items: center;
            text-decoration: none;
        }
        .logo-text {
            color: #333333;
            font-weight: bold;
            font-size: ${rem(20)};
            margin-left: ${rem(20)};
        }
    }
`

export default Header
