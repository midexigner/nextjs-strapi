## Next.js And #Strapi
- node -v
- npx create-next-app
- cd nextjs-strapi
- create file `next.config.js`
```sh
const path = require('path')

module.exports ={
    webpack:config =>{
        config.resolve.alias['components'] = path.join(__dirname,'components')
        config.resolve.alias['public'] = path.join(__dirname,'public')
        return config
    }
}
```
- create `component` folder inside a create `Header.js`


```sh

```
- create `pages` folder inside a open `_app.js`
```sh
import Header from 'components/Header'
import '../styles/globals.css'

function MyApp({ Component, pageProps }) {
  return (
<>
<Header />
<Component {...pageProps} />
</>

  )
}

export default MyApp

```
- create `pages` folder inside a open `_document.js`
```sh
import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    return (
      <Html>
        <Head>
          <link rel='icon' href="/favicon.io" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
```

- npm i @emotion/core
- npm i @emotion/styled
- npm i emotion-theming
- npm install --save polished

- create `.env` file
```sh
API_URL=http://localhost:1337/
```
- npm install dotenv
- npm install isomorphic-unfetch

```sh
  <link href="https://fonts.googleapis.com/css2?family=Libre+Baskerville:wght@400;700&display=swap" rel="stylesheet"/>
```


- 
- npm install @emotion/core @emotion/styled emotion-theming
- https://emotion.sh/docs/introduction
- https://rebassjs.org/
- https://rebassjs.org/reflexbox/
- https://github.com/CleverProgrammers/robinhood-clone
- https://www.youtube.com/watch?v=Xwchwt8TIpk&list=PLUBR53Dw-Ef_oTLzPB3G5CdLWnGOSsec3&index=3&ab_channel=WatchandLearn
- https://www.youtube.com/watch?v=plRcoRqLriw&list=PL4-IK0AVhVjPv5tfS82UF_iQgFp4Bl998&ab_channel=KevinPowell
- https://www.youtube.com/watch?v=cYqbnHlm2zs&ab_channel=ILive4Coding
- 


